package de.niheu.marklogicrepository.configuration;

import com.marklogic.client.DatabaseClient;
import com.marklogic.client.DatabaseClientFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class DatabaseClientConfiguration {
    @Bean
    public DatabaseClient client() {
        return DatabaseClientFactory.newClient(
                "localhost",
                8000,
                new DatabaseClientFactory.DigestAuthContext("admin", "admin")
        );
    }
}
