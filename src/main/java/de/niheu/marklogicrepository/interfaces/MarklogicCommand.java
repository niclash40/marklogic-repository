package de.niheu.marklogicrepository.interfaces;

import de.niheu.marklogicrepository.core.Customer;
import de.niheu.marklogicrepository.repository.marklogic.MarklogicRepository;
import org.instancio.Instancio;
import org.springframework.shell.standard.ShellComponent;
import org.springframework.shell.standard.ShellMethod;
import org.springframework.util.StopWatch;

import java.util.ArrayList;
import java.util.List;

@ShellComponent
public class MarklogicCommand {
    private final MarklogicRepository<Customer, Long> repository;

    public MarklogicCommand(MarklogicRepository<Customer, Long> repository) {
        this.repository = repository;
    }

    @ShellMethod(key = "test")
    public List<Customer> test() {
        return Instancio.ofList(Customer.class).size(10).create();
    }

    @ShellMethod(key = "create")
    public String create() {
        List<Customer> customers = Instancio.ofList(Customer.class).size(10).create();

        StopWatch stopWatch = new StopWatch();
        stopWatch.start();

        repository.saveAll(customers);

        stopWatch.stop();
        System.out.println("Erstellen dauerte: " + stopWatch.getTotalTimeSeconds() + " Sekunden");
        return "fertig";
    }

    @ShellMethod(key = "findAll")
    public List<Customer> findAll() {
        List<Customer> customers = new ArrayList<>();
        repository.findAll().forEach(customers::add);
        return customers;
    }
}
