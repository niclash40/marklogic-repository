package de.niheu.marklogicrepository.core;

import de.niheu.marklogicrepository.repository.marklogic.annotations.Entity;
import de.niheu.marklogicrepository.repository.marklogic.annotations.Id;

@Entity(
        uri = "/customers/customer",
        collections = {"customer"}
)
public class Customer {
    @Id
    private Long id;
    private String firstName;
    private String lastName;
    private int age;

    public Customer() {}

    public Customer(Long id, String firstName, String lastName, int age) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.age = age;
    }

    public Long getId() {
        return id;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public int getAge() {
        return age;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setAge(int age) {
        this.age = age;
    }
}
