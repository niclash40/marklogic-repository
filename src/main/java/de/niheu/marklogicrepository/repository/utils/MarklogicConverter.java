package de.niheu.marklogicrepository.repository.utils;

import com.fasterxml.jackson.databind.JsonNode;
import de.niheu.marklogicrepository.repository.marklogic.JsonMarklogicEntity;
import de.niheu.marklogicrepository.repository.marklogic.MarklogicEntity;

public interface MarklogicConverter<T, V extends MarklogicEntity<?>> {
    public V convertToMarklogicEntity(T entity);
    public T convertFromMarklogicEntity(JsonMarklogicEntity content, Class<T> entityClass);
}
