package de.niheu.marklogicrepository.repository.utils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import de.niheu.marklogicrepository.repository.marklogic.JsonMarklogicEntity;
import de.niheu.marklogicrepository.repository.marklogic.annotations.MarklogicAnnotationParsable;
import de.niheu.marklogicrepository.repository.marklogic.annotations.MarklogicAnnotationParser;
import org.springframework.stereotype.Component;


@Component
public class JsonMarklogicConverter<T> implements MarklogicConverter<T, JsonMarklogicEntity> {
    private final static String JSON_EXTENSION = ".json";
    private final ObjectMapper mapper = new ObjectMapper();

    public JsonMarklogicConverter() {
    }

    public JsonMarklogicEntity convertToMarklogicEntity(T entity) {
        MarklogicAnnotationParser<T> annotationParser = new MarklogicAnnotationParser<>(entity);
        String uri = annotationParser.getUri() + annotationParser.getId() + JSON_EXTENSION;
        String[] collections = annotationParser.getCollections();
        JsonNode jsonNode = mapper.valueToTree(entity);

        return new JsonMarklogicEntity(uri, jsonNode, collections);
    }

    @Override
    public T convertFromMarklogicEntity(JsonMarklogicEntity content, Class<T> entityClass) {
        try {
            return mapper.treeToValue(content.getEntity(), entityClass);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }
}

