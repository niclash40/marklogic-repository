package de.niheu.marklogicrepository.repository.customer;

import com.marklogic.client.DatabaseClient;
import de.niheu.marklogicrepository.core.Customer;
import de.niheu.marklogicrepository.repository.marklogic.JsonMarklogicRepository;
import org.springframework.stereotype.Repository;

@Repository
public class CustomerRepository extends JsonMarklogicRepository<Customer, Long> {

    protected CustomerRepository(DatabaseClient client) {
        super(client);
    }
}
