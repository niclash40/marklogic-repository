package de.niheu.marklogicrepository.repository.marklogic;

import java.util.Optional;

public interface MarklogicRepository<T, ID> {
    Iterable<T> findAll();
    <S extends T> void saveAll(Iterable<S> entities);
}
