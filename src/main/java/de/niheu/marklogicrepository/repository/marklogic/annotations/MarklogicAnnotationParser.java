package de.niheu.marklogicrepository.repository.marklogic.annotations;

import org.springframework.stereotype.Component;

import java.lang.reflect.Field;

public class MarklogicAnnotationParser<T> implements MarklogicAnnotationParsable<T> {

    private String uri;
    private String[] collections;
    private Long id;

    public MarklogicAnnotationParser(T entity) {
        initAttributesWithParsing(entity);
    }

    private void initAttributesWithParsing(T entity) {
        Class<?> entityClass = entity.getClass();
        parseIdFrom(entityClass);

        Entity entityAnnotation = entityClass.getAnnotation(Entity.class);
        if (entityAnnotation == null) {
            throw new IllegalArgumentException("No MarklogicEntity annotation found for class " + entityClass.getName());
        }
        this.uri = entityAnnotation.uri();
        this.collections = entityAnnotation.collections();
    }

    private void parseIdFrom(Class<?> entityClass) {
        Long id = 0L;
        Field[] fields = entityClass.getDeclaredFields();
        for (Field field : fields) {
            if (field.isAnnotationPresent(Id.class)) {
                field.setAccessible(true);
                try {
                    id = (Long) field.get(entityClass);
                } catch (IllegalAccessException e) {
                    throw new IllegalArgumentException("No MarklogicId annotation found for class " + entityClass.getName());
                }
                break;
            }
        }

        this.id = id;
    }

    @Override
    public String getUri() {
        return null;
    }

    @Override
    public String[] getCollections() {
        return new String[0];
    }

    @Override
    public Long getId() {
        return null;
    }
}
