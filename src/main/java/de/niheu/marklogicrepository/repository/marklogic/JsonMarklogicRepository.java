package de.niheu.marklogicrepository.repository.marklogic;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.marklogic.client.DatabaseClient;
import com.marklogic.client.document.DocumentPage;
import com.marklogic.client.document.DocumentRecord;
import com.marklogic.client.document.DocumentWriteSet;
import com.marklogic.client.document.JSONDocumentManager;
import com.marklogic.client.io.JacksonHandle;
import com.marklogic.client.query.StructuredQueryBuilder;
import com.marklogic.client.query.StructuredQueryDefinition;
import de.niheu.marklogicrepository.repository.utils.JsonMarklogicConverter;
import de.niheu.marklogicrepository.repository.utils.MarklogicConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.lang.reflect.ParameterizedType;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

@Repository
public abstract class JsonMarklogicRepository<T, ID> implements MarklogicRepository<T, ID> {
    private final DatabaseClient client;
    private final JSONDocumentManager documentManager;
    private final static int BATCH_SIZE = 1000;
    private final ObjectMapper mapper;
    private MarklogicConverter<T, JsonMarklogicEntity> jsonMarklogicConverter;
    private final Class<T> entityClass;

    protected JsonMarklogicRepository(DatabaseClient client) {
        this.client = client;
        this.documentManager = client.newJSONDocumentManager();
        this.mapper = new ObjectMapper();

        // Reflection, um den Typ T zu erhalten
        ParameterizedType superClass = (ParameterizedType) getClass().getGenericSuperclass();
        this.entityClass = (Class<T>) superClass.getActualTypeArguments()[0];
    }

    @Autowired
    protected void setJsonMarklogicConverter(JsonMarklogicConverter<T> jsonMarklogicConverter) {
        this.jsonMarklogicConverter = jsonMarklogicConverter;
    }

    @Override
    public Iterable<T> findAll() {
        List<T> resultList = new ArrayList<>();
        StructuredQueryBuilder qb = new StructuredQueryBuilder();
        StructuredQueryDefinition queryDef = qb.collection("customer");
        DocumentPage documentPage = documentManager.search(queryDef, 1);

        do {
            Iterator<DocumentRecord> recordIterator = documentPage.iterator();

            while (recordIterator.hasNext()) {
                DocumentRecord documentRecord = recordIterator.next();
                JacksonHandle contentHandle = new JacksonHandle();
                documentRecord.getContent(contentHandle);
                JsonNode content = contentHandle.get();
                T entity = jsonMarklogicConverter.convertFromMarklogicEntity(new JsonMarklogicEntity(null, content), entityClass);
                resultList.add(entity);
            }
        } while (documentPage.hasNext());

        return resultList;
    }

    @Override
    public <S extends T> void saveAll(Iterable<S> entities) {
        DocumentWriteSet writeSet = documentManager.newWriteSet();
        Iterator<S> entityIterator = entities.iterator();

        while (entityIterator.hasNext()) {
            for (int i = 0; i < BATCH_SIZE && entityIterator.hasNext(); i++) {
                T entity = entityIterator.next();
                JsonMarklogicEntity marklogicEntity = jsonMarklogicConverter.convertToMarklogicEntity(entity);
                writeSet.add(marklogicEntity.getUri(), marklogicEntity.getMetadataHandle(), marklogicEntity.getJacksonHandle());
            }
            documentManager.write(writeSet);
        }
    }
}
