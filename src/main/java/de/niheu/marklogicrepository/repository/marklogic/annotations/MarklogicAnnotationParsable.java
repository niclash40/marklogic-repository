package de.niheu.marklogicrepository.repository.marklogic.annotations;

public interface MarklogicAnnotationParsable<T> {
    public String getUri();
    public String[] getCollections();
    public Long getId();
}
