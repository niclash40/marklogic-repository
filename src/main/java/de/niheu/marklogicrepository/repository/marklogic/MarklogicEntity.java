package de.niheu.marklogicrepository.repository.marklogic;

import com.marklogic.client.io.DocumentMetadataHandle;
import com.marklogic.client.io.marker.AbstractWriteHandle;

public abstract class MarklogicEntity<T> {
    private final String uri;
    private final T entity;
    private final DocumentMetadataHandle metadataHandle;

    public MarklogicEntity(String uri, T entity, String... collections) {
        this.uri = uri;
        this.entity = entity;
        this.metadataHandle = new DocumentMetadataHandle();
        this.addToCollections(collections);
    }

    public void addToCollections(String... collections) {
        this.metadataHandle.getCollections().addAll(collections);
    }

    public String getUri() {
        return uri;
    }

    public DocumentMetadataHandle getMetadataHandle() {
        return metadataHandle;
    }

    public abstract AbstractWriteHandle getWriteHandle();

    public T getEntity() {
        return this.entity;
    }
}
