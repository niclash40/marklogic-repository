package de.niheu.marklogicrepository.repository.marklogic;

import com.fasterxml.jackson.databind.JsonNode;
import com.marklogic.client.io.JacksonHandle;
import com.marklogic.client.io.marker.AbstractWriteHandle;

public class JsonMarklogicEntity extends MarklogicEntity<JsonNode> {

    public JsonMarklogicEntity(String uri, JsonNode entity, String... collections) {
        super(uri, entity, collections);
    }

    @Override
    public AbstractWriteHandle getWriteHandle() {
        return null;
    }

    public JacksonHandle getJacksonHandle() {
        return new JacksonHandle(this.getEntity());
    }
}
