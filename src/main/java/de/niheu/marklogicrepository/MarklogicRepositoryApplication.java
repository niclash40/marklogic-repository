package de.niheu.marklogicrepository;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MarklogicRepositoryApplication {

	public static void main(String[] args) {
		SpringApplication.run(MarklogicRepositoryApplication.class, args);
	}

}
