package de.niheu.marklogicrepository;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.test.context.TestConfiguration;

@TestConfiguration(proxyBeanMethods = false)
public class TestMarklogicRepositoryApplication {

	public static void main(String[] args) {
		SpringApplication.from(MarklogicRepositoryApplication::main).with(TestMarklogicRepositoryApplication.class).run(args);
	}

}
